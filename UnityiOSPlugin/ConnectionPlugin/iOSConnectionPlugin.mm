#include "iOSConnectionPlugin.h"

BOOL connected()
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

int Get62()
{
    return 62;
}
