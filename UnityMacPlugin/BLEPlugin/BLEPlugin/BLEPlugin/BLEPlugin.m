// BLEPlugin.m
#import "BLEPlugin.h"
#import "BLEScanner.h"

CallbackDelegate globalCallback;

BLEScanner *scanner;

int UnityCallbackAssignMethod(CallbackDelegate callback)
{
    globalCallback = callback;
    return 1;
}

void StartBLEScan()
{
    if (!scanner)
    {
        scanner = [[BLEScanner alloc] init];
    }
    [scanner startScanning];
}

void PluginConnectToDevice(const char* deviceId)
{
    if (deviceId == NULL)
    {
        NSLog(@"BLEPlugin::PluginConnectToDevice - deviceId is NULL");
        return;
    }

    NSString *deviceIdStr = [NSString stringWithUTF8String:deviceId];
    if (deviceIdStr == nil)
    {
        NSLog(@"BLEPlugin::PluginConnectToDevice - Failed to convert deviceId to NSString");
        return;
    }
    
    NSLog(@"BLEPlugin::PluginConnectToDevice - Connecting To Device");
    
    @try
    {
        [scanner ConnectToDevice:deviceId];
    }
    @catch (NSException *exception)
    {
        NSLog(@"BLEPlugin::PluginConnectToDevice - Exception: %@", exception.reason);
    }
}

void PluginConnectToCharacteristic(const char* deviceId, const char* serviceUuid, const char* characteristicUuid)
{
    if (deviceId == NULL)
    {
        NSLog(@"BLEPlugin::PluginConnectToDevice - deviceId is NULL");
        return;
    }
    
    [scanner ConnectToCharacteristic:deviceId
                             serviceUuid:serviceUuid
                                characteristicUuid:characteristicUuid];
}

const char **GetDiscoveredDevices()
{
    return [scanner getDiscoveredDevices];
}

void CleanUpPlugin()
{
    scanner = nil;
}
