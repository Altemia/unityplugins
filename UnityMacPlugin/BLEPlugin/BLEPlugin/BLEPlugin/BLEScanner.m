// BLEScanner.m
#import "BLEScanner.h"
#import "BLEPlugin.h" // Include the header file here

@implementation BLEScanner

- (id)init
{
    self = [super init];
    if (self)
    {
        centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        discoveredDevices = [NSMutableArray array];
        characteristicsCache = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)startScanning
{
    [centralManager scanForPeripheralsWithServices:nil options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES}];
    NSLog(@"Started Scanning!");
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state == CBManagerStatePoweredOn)
    {
        [self startScanning];
    }
}

- (NSString *)buildDeviceMessage:(CBPeripheral *)peripheral
{
    NSString *deviceName = peripheral.name;
    NSString *deviceIdentifier = peripheral.identifier;

    NSString *combinedString = [NSString stringWithFormat:@"[Device Found]|%@|%@", deviceIdentifier, deviceName];
    return combinedString;
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI
{
    if (peripheral.name != nil && ![discoveredDevices containsObject:peripheral.name])
    {
        [discoveredDevices addObject:peripheral];
        
        if (globalCallback)
        {
            globalCallback(strdup([[self buildDeviceMessage:peripheral] UTF8String]));
        }
    }
    else if (globalCallback)
    {
        NSString *dicoveredPeripheralMessage = [NSString stringWithFormat:@"Discovered peripheral: %@, RSSI: %@, Advertisement Data: %@", peripheral.name, RSSI, advertisementData];
        NSLog(dicoveredPeripheralMessage);
    }
}

- (const char **)getDiscoveredDevices
{
    if(discoveredDevices != nil)
    {
        NSUInteger count = [discoveredDevices count];
        const char **devices = (const char **)malloc(sizeof(char *) * count);
        for (NSUInteger i = 0; i < count; i++)
        {
            id deviceAtIndex = [discoveredDevices objectAtIndex:i];
            if(deviceAtIndex != nil)
            {
                devices[i] = strdup([[self buildDeviceMessage:deviceAtIndex] UTF8String]);
            }
        }
        return devices;
    }
    return nil;
}

- (void)ConnectToDevice:(const char *)deviceId
{
    NSString *deviceIdAsNSString = [NSString stringWithUTF8String:deviceId];
    NSLog([NSString stringWithFormat:@"BLEScanner::ConnectToDevice - Connecting To Device %@", deviceIdAsNSString]);
    
    if (discoveredDevices == NULL)
    {
        NSLog(@"BLEScanner::ConnectToDevice - discoveredDevices is NULL");
        return;
    }
    
    for (CBPeripheral* peripheral in discoveredDevices)
    {
        if (peripheral == NULL)
        {
            NSLog(@"BLEScanner::ConnectToDevice - peripheral is NULL");
            continue;
        }

        NSUUID *peripheralID = peripheral.identifier;
        if (peripheralID == NULL)
        {
            NSLog(@"BLEScanner::ConnectToDevice - uuid is NULL");
            continue;
        }

        NSString *peripheralIdAsNSString = [peripheralID UUIDString];
        if (peripheralIdAsNSString == NULL)
        {
            NSLog(@"BLEScanner::ConnectToDevice - uuidString is NULL");
            continue;
        }
        
        if ([peripheralIdAsNSString isEqualToString:deviceIdAsNSString])
        {
            self->connectedPeripheral = peripheral;
            self->connectedPeripheral.delegate = self;
            [self->centralManager stopScan];
            [self->centralManager connectPeripheral:peripheral options:nil];
            
            NSString *deviceMessage = @"[Connection Successful]";
            NSString *deviceName = peripheral.name;
            
            NSString *combinedString = [NSString stringWithFormat:@"%@|%@|", deviceMessage, peripheralIdAsNSString, deviceName];
            globalCallback(strdup([combinedString UTF8String]));
            
            break;
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    [peripheral discoverServices:nil];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    for (CBService *service in peripheral.services)
    {
        NSLog([NSString stringWithFormat:@"[DeviceService]|service.UUID: %@|UUIDString: %@", service.UUID, service.UUID.UUIDString]);
        
        [peripheral discoverCharacteristics:nil forService:service];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if (error)
    {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        return;
    }

    NSString *serviceUUID = service.UUID.UUIDString;
    for (CBCharacteristic *characteristic in service.characteristics)
    {
        NSString *characteristicUUID = characteristic.UUID.UUIDString;
        NSString *characteristicProperties = [self propertiesStringForCharacteristic:characteristic];
    
        NSString *key = [NSString stringWithFormat:@"%@-%@", serviceUUID, characteristicUUID];
        self->characteristicsCache[key] = characteristic;
            
        if (globalCallback)
        {
            NSString *combinedString = [NSString stringWithFormat:@"[Discovered Characteristic]|%@|%@|%@|%@|%@", peripheral.identifier, peripheral.name,
                                        serviceUUID,
                                        characteristicUUID, characteristicProperties];
            globalCallback(strdup([combinedString UTF8String]));
        }
    }
}

- (NSString *)propertiesStringForCharacteristic:(CBCharacteristic *)characteristic
{
    NSMutableArray *propertiesArray = [NSMutableArray array];

    if (characteristic.properties & CBCharacteristicPropertyBroadcast) {
        [propertiesArray addObject:@"Broadcast"];
    }
    if (characteristic.properties & CBCharacteristicPropertyRead) {
        [propertiesArray addObject:@"Read"];
    }
    if (characteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) {
        [propertiesArray addObject:@"WriteWithoutResponse"];
    }
    if (characteristic.properties & CBCharacteristicPropertyWrite) {
        [propertiesArray addObject:@"Write"];
    }
    if (characteristic.properties & CBCharacteristicPropertyNotify) {
        [propertiesArray addObject:@"Notify"];
    }
    if (characteristic.properties & CBCharacteristicPropertyIndicate) {
        [propertiesArray addObject:@"Indicate"];
    }
    if (characteristic.properties & CBCharacteristicPropertyAuthenticatedSignedWrites) {
        [propertiesArray addObject:@"AuthenticatedSignedWrites"];
    }
    if (characteristic.properties & CBCharacteristicPropertyExtendedProperties) {
        [propertiesArray addObject:@"ExtendedProperties"];
    }
    if (characteristic.properties & CBCharacteristicPropertyNotifyEncryptionRequired) {
        [propertiesArray addObject:@"NotifyEncryptionRequired"];
    }
    if (characteristic.properties & CBCharacteristicPropertyIndicateEncryptionRequired) {
        [propertiesArray addObject:@"IndicateEncryptionRequired"];
    }

    return [propertiesArray componentsJoinedByString:@", "];
}

- (void)ConnectToCharacteristic:(const char *)deviceId
                        serviceUuid:(const char *)serviceUuid
                 characteristicUuid:(const char *)characteristicUuid
{
    if(connectedPeripheral)
    {
        NSString *serviceUuidStr = [NSString stringWithUTF8String:serviceUuid];
        NSString *characteristicUuidStr = [NSString stringWithUTF8String:characteristicUuid];
        NSString *key = [NSString stringWithFormat:@"%@-%@", serviceUuidStr, characteristicUuidStr];
        CBCharacteristic* characteristic = self->characteristicsCache[key];
        if(characteristic)
        {
            [connectedPeripheral setNotifyValue:YES forCharacteristic:characteristic];
            
            NSString *deviceIdStr = [NSString stringWithUTF8String:deviceId];
            
            NSString *message = [NSString stringWithFormat:@"[Connected To Characteristic]|%@|%@|%@", deviceIdStr, serviceUuidStr, characteristicUuidStr];
            globalCallback(strdup([message UTF8String]));
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A63"]])
    {
        NSData *data = characteristic.value;
        NSLog(@"Raw data: %@", data);
        
        if (data.length >= 2)
        {
            NSString *deviceID = peripheral.identifier.UUIDString;
            NSString *deviceName = peripheral.name;
            uint16_t watts;
            [data getBytes:&watts length:sizeof(watts)];
            NSLog(@"Current Watts: %d", watts);
            
            NSString *message = [NSString stringWithFormat:@"[Watts Updated]|%@|%@|%d", deviceID, deviceName, watts];
            globalCallback(strdup([message UTF8String]));
        }
        else
        {
            NSLog(@"Data length is less than expected: %lu", (unsigned long)data.length);
        }
    }
}

@end
