// BLEScanner.h
#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BLEScanner : NSObject <CBCentralManagerDelegate>
{
    CBCentralManager *centralManager;
    NSMutableArray *discoveredDevices;
    CBPeripheral *connectedPeripheral;
    NSMutableDictionary<NSString *, CBCharacteristic *> *characteristicsCache;
}

- (void)startScanning;
- (const char **)getDiscoveredDevices;
- (void)ConnectToDevice:(const char *) deviceId;
- (void)ConnectToCharacteristic:(const char *)deviceId
                        serviceUuid:(const char *)serviceUuid
                 characteristicUuid:(const char *)characteristicUuid;

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error;

@end
