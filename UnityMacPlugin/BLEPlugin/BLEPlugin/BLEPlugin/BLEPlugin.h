// BLEPlugin.h
#import <Foundation/Foundation.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*CallbackDelegate)(const char *deviceName);
int UnityCallbackAssignMethod(CallbackDelegate callbackDelegate);

extern CallbackDelegate globalCallback;

void StartBLEScan();
const char **GetDiscoveredDevices();
void PluginConnectToDevice(const char* deviceId);
void PluginConnectToCharacteristic(const char* deviceId, const char* serviceUuid, const char* characteristicUuid);
void CleanUpPlugin();

#ifdef __cplusplus
}
#endif
