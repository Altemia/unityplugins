//
//  TestPlugin.cpp
//  UnityMacPlugin
//
//  Created by Dave Andrews on 20/12/2021.
//

#define DllExport __attribute__(( visibility("default") ))

//#include "TestPlugin.hpp"

extern "C"
{
    DllExport const char* PrintHello()
    {
        return "Hello";
    }

    /*void CallUnityMethod (Unity_Callback1 callbackFunc)
    {
        NSLog(@"here");
        callbackFunc(strdup([@"Calling Unity Method Success" UTF8String]));
    }*/
}
