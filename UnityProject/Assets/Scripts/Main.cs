﻿// ------------------------------------------------------------------------------------------------
// Copyright (C) David Andrews
// ------------------------------------------------------------------------------------------------
using UnityEngine;
using System.Runtime.InteropServices;
using System.Threading;

//------------------------------------------------------------------
public class Main : MonoBehaviour
{
    Thread someThread;

#if UNITY_IPHONE
    [DllImport("__Internal")]
    private static extern int Get62();
    [DllImport("__Internal")]
    private static extern bool connected();
#endif

#if UNITY_STANDALONE_WIN
    [DllImport("UnityWindowsCPlugin")]
    private static extern int add(int a, int b);
#elif UNITY_ANDROID
    private ConnectionSystem m_connectionSystem;
#endif

    public static void ThreadProc()
    {
        var what = Application.internetReachability;
    }

    public delegate void Callback(string message);
    [DllImport("TestPlugin")]
    private static extern void CallUnityMethod(Callback UnityMethodCalledFromXcode);

    //------------------------------------------------------------------
    void UnityMethodCalledFromXcode(string message)
    {
        Debug.Log("Message = " + message);
    }

    //------------------------------------------------------------------
    void Awake()
    {
#if UITY_ANDROID
        m_connectionSystem = new ConnectionSystem();
#endif
        //someThread = new Thread(new ThreadStart(ThreadProc));
        //someThread.Start();
    }

    //------------------------------------------------------------------
    private void OnDestroy()
    {
#if UITY_ANDROID
        m_connectionSystem.Release();
#endif
    }

    //------------------------------------------------------------------
    void OnGUI()
    {
#if UNITY_ANDROID
        AndroidOnGUI();
#endif

#if UNITY_IOS
        IOSOnGUI();
#endif

#if UNITY_STANDALONE_WIN
        GUILayout.Label(add(2,6).ToString());
#endif

#if UNITY_STANDALONE_WIN
        //-- C# Plugin
        UnityWindowsPlugin.DebugUtils utils = new UnityWindowsPlugin.DebugUtils();
        GUILayout.Label(utils.Get97().ToString());
#endif
    }

#if UNITY_IOS
    //------------------------------------------------------------------
    private void IOSOnGUI()
    {
        GUILayout.Label("iOS");
        GUILayout.Label("Test: " + Get62());
        GUILayout.Label("IsConnected: " + connected().ToString());
    }
#endif

#if UNITY_ANDROID
    //------------------------------------------------------------------
    private void AndroidOnGUI()
    {
        GUILayout.Label("Android");
        GUILayout.Label("Connection Status: " + m_connectionSystem.GetCurrentConnectionType());
    }
#endif
}
