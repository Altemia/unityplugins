﻿// ------------------------------------------------------------------------------------------------
// Copyright (C) David Andrews
// ------------------------------------------------------------------------------------------------
using UnityEngine;

//------------------------------------------------------------------
public class ConnectionSystem
{
    private ConnectionCallbacks m_connectionCallbacks;

    private string m_currentConnection = "Invalid";

    //------------------------------------------------------------------
    public ConnectionSystem()
    {
        Init();
    }

    //------------------------------------------------------------------
    private void Init()
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject context = activity.Call<AndroidJavaObject>("getApplicationContext");

        m_connectionCallbacks = new ConnectionCallbacks();
        ConnectionCallbacks.connectionCallback += ConnectionCallback;

        var javaConnectionSystem = new AndroidJavaObject("com.davidandrews.connectionplugin.ConnectionSystem");
        javaConnectionSystem.CallStatic("SetContext", context, m_connectionCallbacks);
    }

    //------------------------------------------------------------------
    private void ConnectionCallback(string in_connectionType)
    {
        m_currentConnection = in_connectionType;
    }

    //------------------------------------------------------------------
    public string PluginTest()
    {
        var testAndroidObj = new AndroidJavaObject("com.davidandrews.connectionplugin.ConnectionSystem");
        var pids = testAndroidObj.CallStatic<string>("PluginTest");
        return pids;
    }

    //------------------------------------------------------------------
    public string GetCurrentConnectionType()
    {
        return m_currentConnection;
    }

    //------------------------------------------------------------------
    public void Release()
    {
        ConnectionCallbacks.connectionCallback -= ConnectionCallback;
    }
}
