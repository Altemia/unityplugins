﻿// ------------------------------------------------------------------------------------------------
// Copyright (C) David Andrews
// ------------------------------------------------------------------------------------------------
using UnityEngine;
using System;

//------------------------------------------------------------------
public class ConnectionCallbacks : AndroidJavaProxy
{
    public Action GotJavaCallback;

    public delegate void ConnectionCallbackDelegate(string in_connectionType);
    public static ConnectionCallbackDelegate connectionCallback;

    //------------------------------------------------------------------
    public ConnectionCallbacks() : base("com.davidandrews.connectionplugin.ConnectionChangeCallback")
    {
    }

    //------------------------------------------------------------------
    // This method will be invoked from the plugin
    //------------------------------------------------------------------
    public void OnConnectionChanged(string in_connectionStatus)
    {
        connectionCallback?.Invoke(in_connectionStatus);
    }
}