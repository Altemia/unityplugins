#if _MSC_VER // this is defined when compiling with Visual Studio
    #define DllExport __declspec(dllexport) // Visual Studio needs annotating exported functions with this
#else
    #define DllExport
#endif

//#include "TestPlugin.hpp"

extern "C"
{
    DllExport const char* PrintHello()
    {
        return "Hello";
    }
}
