// -----------------------------
// Copyright (C) David Andrews
// -----------------------------
#include "pch.h"

#include "DeviceConnectionsManager.h"
#include "IDLTesting.LiteWatcher.h"
#include "UnityCallback.h"

using namespace winrt;
using namespace Windows::Foundation;
using namespace Windows::Devices::Enumeration;
using namespace Windows::Devices::Bluetooth;
using namespace Windows::Devices::Bluetooth::GenericAttributeProfile;
using namespace Windows::Storage::Streams;

DeviceConnectionsManager::DeviceConnectionsManager()
{
}

DeviceConnectionsManager::~DeviceConnectionsManager()
{
    Release();
}

void DeviceConnectionsManager::Initialise()
{
    PrintLog("BLEPlugin::DeviceConnectionsManager::Initialise() - Stage 0\n");

    m_deviceWatcher = winrt::make_self<winrt::IDLTesting::implementation::LiteWatcher>();

    m_deviceWatcher->EnumerateButton_Click();
}

winrt::fire_and_forget DeviceConnectionsManager::UpdateBikeList()
{
    PrintLog("BLEPlugin::DeviceConnectionsManager::UpdateBikeList() - Stage 0\n");

    auto deviceList = m_deviceWatcher->KnownDevices();

    if (deviceList != NULL)
    {
        m_devicesInformation.clear();

        uint32_t size = deviceList.Size();

        PrintLog(("BLEPlugin::DeviceConnectionsManager::UpdateBikeList() - Stage 1 - " + std::to_string(size) + " devices\n").c_str());

        for (uint32_t index = 0; index < size; ++index)
        {
            if (index >= deviceList.Size())
            {
                break;
            }

            auto inspectDevice = deviceList.GetAt(index);
            if (inspectDevice != NULL)
            {
                auto bluetoothLEDeviceDisplay = inspectDevice.as<winrt::IDLTesting::BluetoothLEDeviceDisplay>();
                if (bluetoothLEDeviceDisplay != NULL)
                {
                    auto bluetoothLEDeviceInformation = bluetoothLEDeviceDisplay.DeviceInformation();
                    auto bluetoothLEDeviceID = bluetoothLEDeviceInformation.Id();
                    auto bluetoothLeDevice = co_await BluetoothLEDevice::FromIdAsync(bluetoothLEDeviceID);

                    if (bluetoothLeDevice != NULL)
                    {
                        m_devicesInformation.push_back(bluetoothLEDeviceInformation);
                    }
                    else
                    {
                        PrintLog(("main.cpp: Failed to create device from UUID: " + to_string(bluetoothLEDeviceID.c_str()) +  "\nCheck bluetooth is turned on\n").c_str());
                    }
                }
            }
        }

        PrintLog("BLEPlugin::DeviceConnectionsManager::UpdateBikeList() - Stage 2 - Device loop finished\n");

        PrintDeviceInformation();
    }
    else
    {
        PrintLog("BLEPlugin::DeviceConnectionsManager::UpdateBikeList() - Stage 1 - Fail to update bike list: deviceList is NULL\n");
    }
}

void DeviceConnectionsManager::PrintDeviceInformation()
{
    if (m_devicesInformation.size() > 0)
    {
        for (auto it = begin(m_devicesInformation); it != end(m_devicesInformation); ++it)
        {
            PluginCallback(("[Device Found]|" + to_string(it->Id()) + "|" + to_string(it->Name())).c_str());
        }
    }
    else
    {
        PrintLog("BLEPlugin::DeviceConnectionsManager::PrintDeviceInformation() - No devices found!\n");
    }
}

void DeviceConnectionsManager::RefreshDeviceList()
{
    UpdateBikeList();
}

winrt::fire_and_forget DeviceConnectionsManager::ConnectDevice(winrt::hstring const& deviceID)
{
    bool result = co_await m_deviceWatcher->SubscribeToPowerData(deviceID);
    if(!result)
    {
        PrintLog(("BLEPlugin::DeviceConnectionsManager::ConnectDevice - Unable to connect to " + winrt::to_string(deviceID) + "\n").c_str());
    }
}

void DeviceConnectionsManager::Release()
{
    m_deviceWatcher = nullptr;
}