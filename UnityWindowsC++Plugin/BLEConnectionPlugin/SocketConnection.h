#pragma once

#include "pch.h"

#include "IDLTesting/IDLTesting.LiteWatcher.h"

class SocketConnection
{
public:
    SocketConnection();

    void Initialise();

    bool HandleIncomingEvents();

    void Release();

private:
    void HandleError(const char* message);

    void CleanupSocket();

private:
    WSAEVENT m_socketEvents;
    SOCKET m_socket;
    WSANETWORKEVENTS m_nextworkEvents;
};