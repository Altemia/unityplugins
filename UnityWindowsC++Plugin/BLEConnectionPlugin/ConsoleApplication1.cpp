// -----------------------------
// Copyright (C) David Andrews
// -----------------------------
#include "pch.h"

#include <iostream>
#include "BLEConnectionPlugin.h"
#include "UnityCallback.h"

#define CONSOLE_APP 0
#define DLL_LIBRARY 1

static BLEConnectionPlugin Plugin;

#if DLL_LIBRARY
    #if _MSC_VER // this is defined when compiling with Visual Studio
        #define DllExport __declspec(dllexport) // Visual Studio needs annotating exported functions with this
    #else
        #define DllExport
    #endif

extern "C"
{
    DllExport int UnityCallbackAssignMethod(UNITYLOGCALLBACK callback)
    {
        UnityCallbackManager::getInstance().UnityLogCallback = callback;
        if (callback)
        {
            return 1;
        }
        return 0;
    }

    DllExport const void InitialisePlugin()
    {
        Plugin.Start();
    }

    DllExport const void PluginRefreshDeviceList()
    {
        Plugin.RefreshDeviceList();
    }

    DllExport const void ConnectDevice(const char* deviceID)
    {
        PrintLog(("BLEPlugin::ConnectDevice - Attempting to connect to " + std::string(deviceID) + "\n").c_str());
        Plugin.ConnectDevice(deviceID);
    }

    DllExport const void ReleasePlugin()
    {
        Plugin.Release();
    }
}
#endif

#if CONSOLE_APP
BOOL WINAPI ConsoleHandlerRoutine(DWORD dwCtrlType)
{
    if (CTRL_CLOSE_EVENT == dwCtrlType)
    {
        Plugin.Release();
        return TRUE;
    }

    return FALSE;
}

int main()
{
    std::cout << "Main - Start\n";

    if (FALSE == SetConsoleCtrlHandler(ConsoleHandlerRoutine, TRUE))
    {
        PrintLog("Unable to register console close callback!");
    }

    Plugin.Start();
    Plugin.RefreshDeviceList();
    while (1)
    {
        std::cin.get();
        Plugin.RefreshDeviceList();
    }
    Plugin.Release();
}
#endif