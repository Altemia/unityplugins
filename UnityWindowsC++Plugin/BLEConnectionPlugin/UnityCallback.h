// -----------------------------
// Copyright (C) David Andrews
// -----------------------------
#pragma once
#include "pch.h"

typedef void(__stdcall* UNITYLOGCALLBACK)(const char*);

class UnityCallbackManager
{
public:
	static UnityCallbackManager& getInstance()
	{
		static UnityCallbackManager instance;
		return instance;
	}

	UNITYLOGCALLBACK UnityLogCallback;
};

static void PluginCallback(const char* message)
{
	auto callback = UnityCallbackManager::getInstance().UnityLogCallback;
	if (callback)
	{
		callback(message);
	}
	else
	{
		printf(message);
	}
}

static void PrintLog(const char* message)
{
	printf(message);
}