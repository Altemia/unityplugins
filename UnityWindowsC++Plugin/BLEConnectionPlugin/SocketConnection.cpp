#include "pch.h"

#include "SocketConnection.h"

SocketConnection::SocketConnection()
{
}

void SocketConnection::Initialise()
{
    printf("Socket startup\n");

    // We want version 2.2.
    WSADATA w;
    int error = WSAStartup(0x0202, &w);
    if (error != 0)
    {
        throw "WSAStartup failed";
    }
    if (w.wVersion != 0x0202)
    {
        throw "Wrong WinSock version";
    }

    //printf("Server starting\n");

    //Build socket address structure for binding the socket
    //sockaddr_in InetAddr;
    //InetAddr.sin_family = AF_INET;
    //inet_pton(AF_INET, SERVERIP, &(InetAddr.sin_addr));
    //InetAddr.sin_port = htons(SERVERPORT);

    ////Create our TCP server/listen socket
    //ListenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    //if (ListenSocket == INVALID_SOCKET)
    //{
    //    HandleError("socket failed");
    //}

    //// Bind the server socket to its address.
    //if (bind(ListenSocket, (SOCKADDR*)&InetAddr, sizeof(InetAddr)) != 0)
    //{
    //    HandleError("bind failed");
    //}

    //// Create a new event for checking listen socket activity
    //ListenEvent = WSACreateEvent();
    //if (ListenEvent == WSA_INVALID_EVENT)
    //{
    //    HandleError("server event creation failed");
    //}
    ////Assosciate this event with the socket types we're interested in
    ////In this case, on the server, we're interested in Accepts and Closes
    //WSAEventSelect(ListenSocket, ListenEvent, FD_ACCEPT | FD_CLOSE);

    ////Start listening for connection requests on the socket
    //if (listen(ListenSocket, 1) == SOCKET_ERROR) 
    //{
    //    HandleError("listen failed");
    //}

    //eventCount++;

    //printf("Listening on socket...\n");
    printf("Winsock initialised\n");
}

bool SocketConnection::HandleIncomingEvents()
{
    return false;

    DWORD returnVal = WSAWaitForMultipleEvents(1, &m_socketEvents, false, 0, false); //Check if any network events allow winsock functions to run without blocking
    if ((returnVal != WSA_WAIT_TIMEOUT) && (returnVal != WSA_WAIT_FAILED))
    {
        if (WSAEnumNetworkEvents(m_socket, m_socketEvents, &m_nextworkEvents) == SOCKET_ERROR)
        {
            Release();
            //Get the network events as binary flags
            HandleError("Retrieving event information failed");
        }
        if (m_nextworkEvents.lNetworkEvents & FD_CLOSE)	//Server has closed the connection
        {
            printf("Server closed the connection\n");

            //We ignore the error if the client just force quit
            if (m_nextworkEvents.iErrorCode[FD_CLOSE_BIT] != 0 && m_nextworkEvents.iErrorCode[FD_CLOSE_BIT] != 10053)
            {
                printf("FD_CLOSE failed with error %d\n", m_nextworkEvents.iErrorCode[FD_CLOSE_BIT]);

                return false;
            }

            Release();
            return false;
        }
        else if (m_nextworkEvents.lNetworkEvents & FD_READ)	//Server has sent data to read
        {
            if (m_nextworkEvents.iErrorCode[FD_READ_BIT] != 0)
            {
                printf("FD_ACCEPT failed with error %d\n", m_nextworkEvents.iErrorCode[FD_READ_BIT]);
                return false;
            }

            //UpdateBikeList();
        }
    }
    else if (returnVal == WSA_WAIT_TIMEOUT)
    {
        //All good, we just have no activity
    }
    else if (returnVal == WSA_WAIT_FAILED)
    {
        Release();
        HandleError("WSAWaitForMultipleEvents failed!");
    }

    return true;
}

void SocketConnection::HandleError(const char* message)
{
    Release();
    throw message;
}

void SocketConnection::CleanupSocket()
{
    if (closesocket(m_socket) != SOCKET_ERROR)
    {
        printf("Successfully closed Connected socket\n");
    }
    if (m_socketEvents != NULL && WSACloseEvent(m_socketEvents) == false)
    {
        HandleError("WSACloseEvent() failed");
    }
}

void SocketConnection::Release()
{
    CleanupSocket();
}