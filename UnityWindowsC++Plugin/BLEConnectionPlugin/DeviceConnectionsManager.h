// -----------------------------
// Copyright (C) David Andrews
// -----------------------------
#pragma once

#include "pch.h"

#include "IDLTesting.LiteWatcher.h"
#include <winrt/Windows.Foundation.h>

class DeviceConnectionsManager
{
public:
    DeviceConnectionsManager();
    ~DeviceConnectionsManager();

    void Initialise();

    winrt::fire_and_forget UpdateBikeList();

    void PrintDeviceInformation();

    void RefreshDeviceList();

    winrt::fire_and_forget ConnectDevice(winrt::hstring const& deviceID);

    void Release();

private:
    winrt::Windows::Foundation::Collections::IObservableVector<winrt::Windows::Foundation::IInspectable>* m_deviceList;
    winrt::com_ptr<winrt::IDLTesting::implementation::LiteWatcher> m_deviceWatcher;

    std::vector<winrt::Windows::Devices::Enumeration::DeviceInformation> m_devicesInformation;
};