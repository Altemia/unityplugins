// -----------------------------
// Copyright (C) David Andrews
// -----------------------------
#pragma once

#include "DeviceConnectionsManager.h"

class BLEConnectionPlugin
{
public:
    BLEConnectionPlugin();
    ~BLEConnectionPlugin();

    void Start();

    void RefreshDeviceList();

    void ConnectDevice(const char* deviceID);

    void Release();

private:
    DeviceConnectionsManager m_deviceConnectionsManager;
};