// -----------------------------
// Copyright (C) David Andrews
// -----------------------------
#include "pch.h"

#include "BLEConnectionPlugin.h"
#include <winrt/Windows.Foundation.h>

BLEConnectionPlugin::BLEConnectionPlugin()
{
}

BLEConnectionPlugin::~BLEConnectionPlugin()
{
    m_deviceConnectionsManager.Release();
}

void BLEConnectionPlugin::Start()
{
    m_deviceConnectionsManager.Initialise();
}

void BLEConnectionPlugin::RefreshDeviceList()
{
    m_deviceConnectionsManager.RefreshDeviceList();
}

void BLEConnectionPlugin::ConnectDevice(const char* deviceID)
{
    m_deviceConnectionsManager.ConnectDevice(winrt::to_hstring(deviceID));
}

void BLEConnectionPlugin::Release()
{
    m_deviceConnectionsManager.Release();
}