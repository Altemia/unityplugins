package com.davidandrews.profilingplugin;

import android.app.*;
import android.os.*;

import java.text.DecimalFormat;

public class ProfilingSystem
{
    public static final long BytesInMB = 1024L * 1024L;
    public static final long BytesInKB = 1024L;

    public int[] getMemoryDetailInfo()
    {
        StringBuilder buff = new StringBuilder();

        int[] pids = new int[1];
        pids[0] = android.os.Process.myPid();
        return pids;
    }

    public static String getVMHeapInfo()
    {
        long maxMem = Runtime.getRuntime().maxMemory();
        long freeMem = Runtime.getRuntime().freeMemory();
        long totalMem = Runtime.getRuntime().totalMemory();
        long allocatedMem = totalMem - freeMem;
        double ratio = allocatedMem * 100L / totalMem;
        String info = String.format("VM Heap Mem : total=%s, allocated=%s (%s %%), max=%s",
                new Object[] { byteToMB(totalMem), byteToMB(allocatedMem), new DecimalFormat("##.#").format(ratio),
                        byteToMB(maxMem) });
        return info;
    }

    public static String getNativeHeapInfo()
    {
        long maxMem = Debug.getNativeHeapSize();
        long allocatedMem = Debug.getNativeHeapAllocatedSize();
        long freeMem = Debug.getNativeHeapFreeSize();
        double ratio = allocatedMem * 100L / maxMem;
        String info = String.format("Native Heap : max=%s, allocated=%s (%s %%), free=%s",
                new Object[] { byteToMB(maxMem), byteToMB(allocatedMem), new DecimalFormat("##.#").format(ratio),
                        byteToMB(freeMem) });
        return info;
    }

    private static String byteToMB(long byteNum)
    {
        byteNum /= BytesInMB;
        DecimalFormat f1 = new DecimalFormat("#,###MB");
        return f1.format(byteNum);
    }

    private static String kbToMB(long kbNum)
    {
        kbNum /= BytesInKB;
        DecimalFormat f1 = new DecimalFormat("#,###MB");
        return f1.format(kbNum);
    }
}