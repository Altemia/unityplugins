// ----------------------------------------
// Copyright (C) David Andrews
// ----------------------------------------
package com.davidandrews.connectionplugin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

//------------------------------------------------------------------
public class ConnectionListener extends BroadcastReceiver
{
    //------------------------------------------------------------------
    @Override
    public void onReceive(Context context, Intent intent)
    {
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction()) ||
                "android.net.wifi.WIFI_STATE_CHANGED".equals(intent.getAction()))
        {
            if(ConnectionSystem.IsInitialised())
            {
                String status = ConnectionSystem.GetConnectionType();
                ConnectionSystem.CallBackInstance().OnConnectionChanged(status);
            }
        }
    }
}
