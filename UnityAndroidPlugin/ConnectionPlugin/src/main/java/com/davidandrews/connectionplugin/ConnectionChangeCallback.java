// ----------------------------------------
// Copyright (C) David Andrews
// ----------------------------------------
package com.davidandrews.connectionplugin;

//------------------------------------------------------------------
public interface ConnectionChangeCallback
{
    void OnConnectionChanged(String in_connectionType);
}
