// ----------------------------------------
// Copyright (C) David Andrews
// ----------------------------------------
package com.davidandrews.connectionplugin;

import android.content.Context;
import android.content.IntentFilter;
import android.net.*;

//------------------------------------------------------------------
public class ConnectionSystem
{
    private static final String k_success = "Success";

    public static final String k_dataConnection = "DataConnection";
    public static final String k_wifiConnection = "WifiConnection";
    public static final String k_noConnection = "Offline";

    private static Context m_context = null;
    private static ConnectivityManager m_connectivityManager;

    private static ConnectionChangeCallback m_callbackInstance;

    private static boolean m_isInitialised = false;

    //------------------------------------------------------------------
    public static void SetContext(Context in_Context, ConnectionChangeCallback in_callbackInstance)
    {
        m_context = in_Context;

        ConnectionListener br = new ConnectionListener();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        m_context.registerReceiver(br, filter);

        m_connectivityManager = (ConnectivityManager) m_context.getSystemService(Context.CONNECTIVITY_SERVICE);

        m_callbackInstance = in_callbackInstance;

        m_isInitialised = true;
    }

    //------------------------------------------------------------------
    public static String PluginTest()
    {
        return k_success;
    }

    //------------------------------------------------------------------
    public static ConnectionChangeCallback CallBackInstance()
    {
        return m_callbackInstance;
    }

    //------------------------------------------------------------------
    public static String GetConnectionType()
    {
        for (Network network : m_connectivityManager.getAllNetworks())
        {
            NetworkInfo networkInfo = m_connectivityManager.getNetworkInfo(network);
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI)
            {
                return k_wifiConnection;
            }
            if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE)
            {
                return k_dataConnection;
            }
        }
        return k_noConnection;
    }

    //------------------------------------------------------------------
    public static boolean IsNetworkAvailable()
    {
        if(m_context != null)
        {
            NetworkInfo activeNetworkInfo = m_connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }
        else
        {
            return false;
        }
    }

    //------------------------------------------------------------------
    public static boolean IsInitialised()
    {
        return m_isInitialised;
    }
}