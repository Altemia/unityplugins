// ----------------------------------------
package com.davidandrews.anrsupervisor;

//------------------------------------------------------------------
public interface ANRDetectedCallback
{
    //------------------------------------------------------------------
    void OnANRDetected(String in_report);
}