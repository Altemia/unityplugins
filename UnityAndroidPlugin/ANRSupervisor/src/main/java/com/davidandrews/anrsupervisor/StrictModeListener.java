package com.davidandrews.anrsupervisor;

import android.os.StrictMode;
import android.os.strictmode.Violation;

import java.util.logging.Level;
import java.util.logging.Logger;

//------------------------------------------------------------------
public class StrictModeListener implements StrictMode.OnThreadViolationListener
{
    String k_tag = "StrictModeListener";

    //------------------------------------------------------------------
    @Override public void onThreadViolation(Violation violation)
    {
        Logger.getLogger(k_tag).log(Level.SEVERE, "Strict mode!!");
    }
}
