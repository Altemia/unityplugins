// ----------------------------------------
package com.davidandrews.anrsupervisor;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.os.Bundle;
import android.view.InputEvent;
import android.view.MotionEvent;
import android.os.StrictMode;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

//------------------------------------------------------------------
// A class supervising the UI thread for ANR errors. Use 
// {@link #start()} and {@link #stop()} to control
// when the UI thread is supervised
//------------------------------------------------------------------
public class ANRSupervisor
{
	public static String k_tag = "[ANR]";

	public static ANRSupervisor instance;

	private static ANRDetectedCallback m_anrDetectedCallback;

	//-- The {@link ANRSupervisorRunnable} running on a separate thread
	public final ANRSupervisorRunnable m_supervisorRunnable;

	//-- The {@link ExecutorService} checking the UI thread
	private ExecutorService m_executor;

	//------------------------------------------------------------------
	public ANRSupervisor(Looper looper, int timeoutCheckDurationSeconds, int checkIntervalSeconds)
	{
		ANRSupervisor.Log("ANRSupervisor::Constructor - Start");
		m_executor = Executors.newSingleThreadExecutor();
		m_supervisorRunnable = new ANRSupervisorRunnable(looper, timeoutCheckDurationSeconds, checkIntervalSeconds);
	}

	//------------------------------------------------------------------
	public static void setTimeCheckSeconds(int in_timeoutCheckSeconds)
	{
		if (instance != null)
		{
			instance.m_supervisorRunnable.setTimeCheckSeconds(in_timeoutCheckSeconds);
		}
	}

	//------------------------------------------------------------------
	public static void setCheckIntervalSeconds(int in_checkIntervalSeconds)
	{
		if (instance != null)
		{
			instance.m_supervisorRunnable.setCheckIntervalSeconds(in_checkIntervalSeconds);
		}
	}

	//------------------------------------------------------------------
	public static void init(ANRDetectedCallback in_listenerCallback)
	{
		setupListener(in_listenerCallback);
		create();
	}

	//------------------------------------------------------------------
	private static void create()
	{
		if (instance == null)
		{
			ANRSupervisor.Log("ANRSupervisor::Create - Creating Main Thread Supervisor");
			// Check for misbehaving SDKs on the main thread.
			int timeoutCheckDurationSeconds = 2;
			int checkIntervalSeconds = 3;
			instance = new ANRSupervisor(Looper.getMainLooper(), timeoutCheckDurationSeconds, checkIntervalSeconds);
		}
	}

	//------------------------------------------------------------------
	// Stets the Unity Listener handle.
	//------------------------------------------------------------------
	private static synchronized void setupListener(ANRDetectedCallback in_listenerCallback)
	{
		ANRSupervisor.Log("ANRSupervisor::setupListener");
		m_anrDetectedCallback = in_listenerCallback;
	}

	//------------------------------------------------------------------
	// Starts the supervision
	//------------------------------------------------------------------
	public static synchronized void start()
	{
		synchronized (instance.m_supervisorRunnable)
		{
			ANRSupervisor.Log("ANRSupervisor::start - Starting Supervisor");
			if (instance.m_supervisorRunnable.isStopped())
			{
				instance.m_executor.execute(instance.m_supervisorRunnable);
			}
			else
			{
				instance.m_supervisorRunnable.resume();
			}
		}
	}

	//------------------------------------------------------------------
	// Stops the supervision. The stop is delayed, so if start() is called right after stop(),
	// both methods will have no effect. There will be at least one more ANR check before the supervision is stopped.
	//------------------------------------------------------------------
	public static synchronized void stop()
	{
		instance.m_supervisorRunnable.stop();
	}

    //------------------------------------------------------------------
    public static ANRDetectedCallback CallbackInstance()
    {
        return m_anrDetectedCallback;
    }

	//------------------------------------------------------------------
	public static String getReport()
	{
		if (instance != null &&
			instance.m_supervisorRunnable != null &&
			instance.m_supervisorRunnable.m_report != null)
		{
			String report = instance.m_supervisorRunnable.m_report;
			instance.m_supervisorRunnable.m_report = null;
			return report;
		}
		return null;
	}

	//------------------------------------------------------------------
	/// Creates a new thread that locks instance.
	/// The main thread then trys to lock instance but can't.
	/// So this is a deadlock
	//------------------------------------------------------------------
	public static synchronized void generateANROnMainThreadTEST()
	{
		ANRSupervisor.Log("ANRSupervisor::generateANROnMainThreadTEST - Creating mutext locked infinite thread");
		new Thread(new Runnable()
		{
			@Override public void run()
			{
				synchronized (instance)
				{
					while (true)
					{
						ANRSupervisor.Log("ANRSupervisor::generateANROnMainThreadTEST - Sleeping for 60 seconds");
						try
						{
							Thread.sleep(60*1000);
						} 
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		}).start();

		ANRSupervisor.Log("ANRSupervisor::generateANROnMainThreadTEST - Running a callback on the main thread that tries to lock the mutex (but can't)");
		new Handler(Looper.getMainLooper()).postDelayed(new Runnable()
		{
			@Override public void run()
			{
				ANRSupervisor.Log("ANRSupervisor::generateANROnMainThreadTEST - Trying to lock the mutex");
				synchronized (instance)
				{
					ANRSupervisor.Log("ANRSupervisor::generateANROnMainThreadTEST - This shouldn't happen");
					throw new IllegalStateException();
				}
			}
		}, 1000);

		ANRSupervisor.Log("ANRSupervisor::generateANROnMainThreadTEST - End of generateANROnMainThreadTEST");
	}

	//------------------------------------------------------------------
	public static void Log(Object log)
	{
		Logger.getLogger(k_tag).log(Level.INFO, "com.DaveA.Debug [ANR] " + log);
	}

}

//------------------------------------------------------------------
// A {@link Runnable} testing the UI thread every X seconds until {@link #stop()} is called
//------------------------------------------------------------------
class ANRSupervisorRunnable implements Runnable
{
	// The {@link Handler} to access the UI threads message queue
	private Handler m_handler;

	// The stop flag
	private boolean m_stopped;

	// Flag indicating the stop was performed
	private boolean m_stopCompleted = true;

	private int m_timeoutCheckSeconds;
	private int m_checkIntervalSeconds;
	
	public String m_report;

	//------------------------------------------------------------------
	public ANRSupervisorRunnable(Looper looper, int timeoutCheckDurationSeconds, int checkIntervalSeconds)
	{
		ANRSupervisor.Log("ANRSupervisorRunnable::Constructor Installing ANR Supervisor on " + looper + " - Interval - " + checkIntervalSeconds + " - timeout (seconds): " + timeoutCheckDurationSeconds);
		m_handler = new Handler(looper);
		m_timeoutCheckSeconds = timeoutCheckDurationSeconds;
		m_checkIntervalSeconds = checkIntervalSeconds;
	}

	//------------------------------------------------------------------
	public void setTimeCheckSeconds(int in_timeoutCheckSeconds)
	{
		m_timeoutCheckSeconds = in_timeoutCheckSeconds;
	}

	//------------------------------------------------------------------
	public void setCheckIntervalSeconds(int in_checkIntervalSeconds)
	{
		m_checkIntervalSeconds = in_checkIntervalSeconds;
	}

	//------------------------------------------------------------------
	@Override public void run()
	{
		this.m_stopCompleted = false;

		// Loop until stop() was called or thread is interrupted
		while (!Thread.interrupted())
		{
			try
			{
				//-- Sleep thread for {m_checkIntervalSeconds} interval between checks
				Thread.sleep(m_checkIntervalSeconds * 1000);

				ANRSupervisor.Log("ANRSupervisorRunnable::run - Check for ANR...");

				// Create new callback
				ANRSupervisorCallback callback = new ANRSupervisorCallback();

				// Perform test, Handler should run the callback within X seconds
				synchronized (callback)
				{
					this.m_handler.post(callback);
					//-- Wait for the callback to be fired, if it takes long than m_timeoutCheckSeconds then we fail the check.
					callback.wait(m_timeoutCheckSeconds * 1000);
					//-- Check if called
					if (!callback.isCalled())
					{
						//-- We have failed the check!
						String message = "ANRSupervisorRunnable::run - Thread " + this.m_handler.getLooper() + " DID NOT respond within " + m_timeoutCheckSeconds + " seconds";
						ANRSupervisor.Log(message);
						ANRSupervisor.CallbackInstance().OnANRDetected(message);

						String report = getProcessJson(this.m_handler.getLooper().getThread());
						ANRSupervisor.Log(report);
						m_report = report;
						ANRSupervisor.CallbackInstance().OnANRDetected(report);

						//-- Check if we've responded yet
						if (callback.isCalled() == false)
						{
							// If the supervised thread still did not respond, quit the app.
							//killApp();
						}
					}
					else
					{
						//-- We have passed the check!
						//-- Log no ANR found.
						//ANRSupervisor.Log("Thread " + this.m_handler.getLooper() + " responded within " + m_timeoutCheckSeconds + " seconds");
					}
				}

				// Check if stopped
				this.checkStopped();
			}
			catch (InterruptedException e)
			{
				ANRSupervisor.Log("ANRSupervisorRunnable::run - Interruption caught.");
				break;
			}
		}

		// Set stop completed flag
		this.m_stopCompleted = true;

		ANRSupervisor.Log("ANRSupervisorRunnable::run - Supervision stopped");
	}

	//------------------------------------------------------------------
	private void killApp()
	{
		ANRSupervisor.Log("ANRSupervisorRunnable::KillApp - Killing my process");
		android.os.Process.killProcess(android.os.Process.myPid());

		ANRSupervisor.Log("ANRSupervisorRunnable::KillApp - Exiting the app");
		System.exit(0); // SNAFU
	}

	//------------------------------------------------------------------
	public String getProcessJson(Thread supervisedThread)
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(bos);

		// Get all stack traces in the system
		Map<Thread, StackTraceElement[]> stackTraces = Thread.getAllStackTraces();
		Locale l = Locale.getDefault();
		
		String deviceName = "";
		try
		{
			android.content.ContentResolver cr = com.unity3d.player.UnityPlayer.currentActivity.getApplicationContext().getContentResolver();
			deviceName = android.provider.Settings.Secure.getString(cr, "device_name");
			if (deviceName == null || deviceName.length() <= 0)
			{
				deviceName = android.provider.Settings.Secure.getString(cr, "bluetooth_name");
			}
		}
		catch (Exception e) {}

		String versionName = "1";
		ps.print(String.format(l, "{\"title\":\"ANR Report\",\"build_version\":\"%s\",\"device\":\"%s\",\"name\":\"%s\",\"callstacks\":[",
			versionName, android.os.Build.FINGERPRINT, deviceName));

		boolean isFirstThread = true;
		for (Thread thread : stackTraces.keySet())
		{
			if (thread == supervisedThread ||
				thread.getName().equals("main") ||
				thread.getName().equals("UnityMain") ||
				thread.getState().equals("BLOCKED"))
			{
				if (isFirstThread)
				{
					isFirstThread = false;
				}
				else
				{ 
					ps.print(",");
				}
				ps.print(String.format(l, "{\"name\":\"%s\",\"state\":\"%s\"", thread.getName(), thread.getState()));
				
				if (thread == supervisedThread)
				{
					ps.print(",\"supervised\":true");
				}

				StackTraceElement[] stack = stackTraces.get(thread);
				if (stack.length > 0)
				{
					ps.print(",\"stack\":[");
					boolean isFirstLine = true;
					for (int i = 0; i < stack.length; ++i)
					{
						if (isFirstLine)
						{ 
							isFirstLine = false;
						}
						else
						{
							ps.print(",");
						}
						StackTraceElement element = stack[i];
						ps.print(String.format(l, "{\"func\":\"%s.%s\",\"file\":\"%s\",\"line\":%d}",
								element.getClassName(),
								element.getMethodName(),
								element.getFileName(), 
								element.getLineNumber()));
					}
					ps.print("]");
				}
				ps.print("}");
			}
		}
		ps.print("]}");

		return new String(bos.toByteArray());
	}

	//------------------------------------------------------------------
	private synchronized void checkStopped() throws InterruptedException
	{
		if (this.m_stopped)
		{
			// Wait 1 second
			Thread.sleep(1000);

			// Break if still stopped
			if (this.m_stopped)
			{
				throw new InterruptedException();
			}
		}
	}

	//------------------------------------------------------------------
	synchronized void stop()
	{
		ANRSupervisor.Log("ANRSupervisorRunnable::stop");
		this.m_stopped = true;
	}

	//------------------------------------------------------------------
	synchronized void resume()
	{
		ANRSupervisor.Log("ANRSupervisorRunnable::resume");
		this.m_stopped = false;
	}

	//------------------------------------------------------------------
	synchronized boolean isStopped()
	{ 
		return this.m_stopCompleted;
	}
}

//------------------------------------------------------------------
// A {@link Runnable} which calls {@link #notifyAll()} when run.
//------------------------------------------------------------------
class ANRSupervisorCallback implements Runnable
{
	private boolean m_called;

	//------------------------------------------------------------------
	public ANRSupervisorCallback()
	{ 
		super();
	}

	//------------------------------------------------------------------
	@Override public synchronized void run()
	{
		this.m_called = true;
		this.notifyAll();
	}

	//------------------------------------------------------------------
	synchronized boolean isCalled()
	{ 
		return this.m_called;
	}
}